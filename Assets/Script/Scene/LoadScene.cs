using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LoadScene : MonoBehaviour
{
    private Animator animator;
    public int intScene;

    private void Start()
    {
        animator = GetComponent<Animator>();
    }

    public void FadeToScene()
    {
        animator.SetTrigger("fade");
        if (intScene == 0)
        {
            Destroy(GameObject.FindGameObjectWithTag("Player"));
        }
    }

    private void OnFadeComplete()
    {
        SceneManager.LoadScene(intScene);
        Debug.Log(intScene);
    }
}
