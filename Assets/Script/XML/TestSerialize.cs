using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Xml.Serialization;

[System.Serializable]
public class TestSerialize 
{
   //[XmlAttribute("id")]
   //public int Id { get; set; }

   //[XmlAttribute("name")]
   //public string Name { get; set; }

   [XmlElement("t")]
   public List<string> List { get; set; }
}
