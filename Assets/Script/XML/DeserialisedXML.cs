using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System.Xml.Serialization;
using TMPro;

public class DeserialisedXML : MonoBehaviour
{
    [SerializeField] private TMP_Text textLabel1;
    [SerializeField] private TMP_Text textLabel2;


    // Start is called before the first frame update
    void Start()
    {
        DeserialiserTest();
    }

    void DeserialiserTest()
    {
        TestSerialize testSerialize = XmlDeSerialize();
        //Debug.Log(testSerialize.Id);
        foreach (string a in testSerialize.List)
        {
            Debug.Log(testSerialize.List[0]);
            Debug.Log(testSerialize.List[1]);
            textLabel1.text = testSerialize.List[0];
            textLabel2.text = testSerialize.List[1];
        }
    }

    TestSerialize XmlDeSerialize()
    {
        FileStream fs = new FileStream(Application.dataPath + "/Resources/slide1.xml", FileMode.Open, FileAccess.ReadWrite, FileShare.ReadWrite);
        XmlSerializer xs = new XmlSerializer(typeof(TestSerialize), new XmlRootAttribute("p-sld"));
        TestSerialize testSerialize = (TestSerialize)xs.Deserialize(fs);
        fs.Close();
        return testSerialize;
    }
}
