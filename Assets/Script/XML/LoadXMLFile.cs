using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System.Xml;
using TMPro;

public class LoadXMLFile : MonoBehaviour
{
    public TextAsset XMLtextAsset;
    public TextMeshProUGUI textUI;


    // Start is called before the first frame update
    void Start()
    {
        string data = XMLtextAsset.text;
        parseXMLFile(data);
    }

    void parseXMLFile(string xmlData)
    {
        string totVal = "";
        XmlDocument xmlDocument = new XmlDocument();
        xmlDocument.Load(new StreamReader(xmlData));

        string xmlPathPattern = "//p:spTree//p:sp//p:nvSpPr//p:txBody//a:p//a:r//a:t";
        XmlNodeList nodeList = xmlDocument.SelectNodes(xmlPathPattern);
        foreach (XmlNode node in nodeList)
        {
            XmlNode txt = node.FirstChild;

            totVal = "Text: " + txt;
            textUI.text = totVal;
        }
    }
}
