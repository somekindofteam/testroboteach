﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class DialogUI : MonoBehaviour
{
    [SerializeField] private TMP_Text textLabel;
    [SerializeField] private AudioSource audioSource;
    [SerializeField] private DialogObject dialogObject;
    public GameObject dialogueBox;
    //[SerializeField] private TestSerialize testSerialize;

    private ResponseHandler responseHandler;
    private TypewriterEffect typewriterEffect;
   

    public bool isOpen { get; set; }

    private void Start()
    {
        typewriterEffect = GetComponent<TypewriterEffect>();
        responseHandler = GetComponent<ResponseHandler>();

        CloseDialogueBox();
        ShowDialogue(dialogObject);
    }

    public void ShowDialogue(DialogObject dialogObject)
    {
        isOpen = true;
        dialogueBox.SetActive(true);

        StartCoroutine(StepThroughDialogue(dialogObject));
        //StartCoroutine(StepThroughDialogue(testSerialize));
    }

    public void AddResponseEvents(ResponseEvent[] responseEvents)
    {
        responseHandler.AddResponseEvents(responseEvents);
    }

    
    private IEnumerator StepThroughDialogue(DialogObject dialogObject)
    {
        for (int i = 0; i < dialogObject.Dialogs.Length; i++)
        {
            AudioClip audio = dialogObject.Dialogs[i].DialogAudio;
            string dialogue = dialogObject.Dialogs[i].DialogText;

            audioSource.clip = audio;
            audioSource.Play();

            yield return RunTypingEffect(dialogue);

            textLabel.text = dialogue;
            

            if (i == dialogObject.Dialogs.Length - 1 && dialogObject.HasResponses)
            {
                break;
            }

            yield return null;
            //yield return new WaitUntil(() => Input.GetKeyDown(KeyCode.Space)); -> Для переключения на следующую часть диалога
            yield return new WaitForSeconds(2);
        }

        if (dialogObject.HasResponses)
        {
            responseHandler.ShowResponses(dialogObject.Responses);
        }else
        {
            CloseDialogueBox();
        }
    }
    

    /*
    private IEnumerator StepThroughDialogue(TestSerialize testSerialize)
    {
        for (int i = 0; i < testSerialize.List.Count; i++)
        {
            //AudioClip audio = dialogObject.Dialogs[i].DialogAudio;
            string dialogue = testSerialize.List[i];

            //audioSource.clip = audio;
            //audioSource.Play();

            yield return RunTypingEffect(dialogue);

            textLabel.text = dialogue;


            if (i == testSerialize.List.Count - 1)
            {
                break;
            } else
            {
                CloseDialogueBox();
            }

            yield return null;
            //yield return new WaitUntil(() => Input.GetKeyDown(KeyCode.Space)); -> Для переключения на следующую часть диалога
            yield return new WaitForSeconds(2);
        }
    }
    */

    private IEnumerator RunTypingEffect(string dialogue)
    {
        typewriterEffect.Run(dialogue, textLabel);

        while (typewriterEffect.isRunning)
        {
            yield return null;

            if (Input.GetKeyDown(KeyCode.Space))
            {
                typewriterEffect.Stop();
            }
        }
    }

    public void CloseDialogueBox()
    {
        isOpen = false;
        dialogueBox.SetActive(false);
        textLabel.text = string.Empty;
    }
}
