using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Dialog 
{
    [SerializeField] [TextArea] private string dialogText;
    [SerializeField] private AudioClip dialogAudio;

    public string DialogText => dialogText;
    public AudioClip DialogAudio => dialogAudio;

}
