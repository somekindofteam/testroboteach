using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class ResponseHandler : MonoBehaviour
{
    //ResponseUI
    [SerializeField] private RectTransform responseBox;
    [SerializeField] private RectTransform responseTemplate;
    [SerializeField] private RectTransform responseContainer;

    private DialogUI dialogUI;
    private ResponseEvent[] responseEvents;

    private List<GameObject> tempResponseButton = new List<GameObject>();

    private void Start()
    {
        dialogUI = GetComponent<DialogUI>();
    }

    public void AddResponseEvents(ResponseEvent[] responseEvents)
    {
        this.responseEvents = responseEvents;
    }

    public void ShowResponses(Response[] responses)
    {
        float responseBoxHeight = 0;

        for (int i = 0; i < responses.Length; i++)
        {
            Response response = responses[i];
            int responseIndex = i;

            GameObject responseButton = Instantiate(responseTemplate.gameObject, responseContainer);
            responseButton.gameObject.SetActive(true);
            responseButton.GetComponent<TMP_Text>().text = response.ResponseText;
            responseButton.GetComponent<Button>().onClick.AddListener(() => OnClickResponse(response, responseIndex));

            tempResponseButton.Add(responseButton);

            responseBoxHeight += responseTemplate.sizeDelta.y;
        }

        responseBox.sizeDelta = new Vector2(responseBox.sizeDelta.x, responseBoxHeight);
        responseBox.gameObject.SetActive(true);
    }

    private void OnClickResponse(Response response, int responseIndex)
    {
        responseBox.gameObject.SetActive(false);

        foreach (GameObject button in tempResponseButton)
        {
            Destroy(button);
        }

        tempResponseButton.Clear();

        if (responseEvents != null && responseIndex <= responseEvents.Length)
        {
            responseEvents[responseIndex].unityEvent?.Invoke();
        }

        dialogUI.ShowDialogue(response.DialogObject);
    }
}
