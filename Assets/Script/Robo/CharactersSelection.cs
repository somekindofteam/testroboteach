using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CharactersSelection : MonoBehaviour
{
    //Select character
    public GameObject characters;
    public int selectedCharacter;

    public int intScene;

    public API api;


    public void StartScene()
    {
        PlayerPrefs.SetInt("selectedCharacter", selectedCharacter);
        SceneManager.LoadScene(intScene);
    }

    public void LoadContent(string name)
    {
        DestroyAllChildren();
        api.GetBundleObject(name, OnContentLoaded, transform);
    }

    void OnContentLoaded(GameObject content)
    {
        //do something cool here
        Debug.Log("Loaded: " + content.name);
        string child = characters.transform.GetChild(0).name;
        Debug.Log(child);
        selectedCharacter = characters.transform.childCount;
    }

    
    void DestroyAllChildren()
    {
        foreach (Transform child in transform)
        {
            //child.gameObject.SetActive(false);
            Destroy(child.gameObject);
        }
    }
    
}
