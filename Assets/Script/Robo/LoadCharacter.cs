using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class LoadCharacter : MonoBehaviour
{
    public GameObject characterPrefabs;
    public Transform spawnPoint;
    public TMP_Text label;
    public GameObject parentCharacters;
    public GameObject clone;
    public GameObject canvasUI;

    private Animator animator;


    // Start is called before the first frame update
    void Start()
    {
        int selectedCharacter = PlayerPrefs.GetInt("selectedCharacter");
        characterPrefabs = GameObject.FindGameObjectWithTag("Player");
        clone = Instantiate(characterPrefabs, spawnPoint.position, Quaternion.identity);
        clone.transform.SetParent(parentCharacters.transform);
        characterPrefabs.SetActive(false);

        label.text = characterPrefabs.name;
        Debug.Log(characterPrefabs.name);

    }

    private void Update()
    {
        bool openUI = canvasUI.GetComponent<DialogUI>().isOpen;

        animator = clone.GetComponent<RoboController>().GetComponent<Animator>();
        //Debug.Log(animator);

        if (openUI == true)
        {
            animator.Play("Talk");
            //Debug.Log(openUI);
        }
        else
        {
            animator.Play("Idle");
            //Debug.Log(openUI);
        }
    }

}
