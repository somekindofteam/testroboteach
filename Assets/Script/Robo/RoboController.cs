using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoboController : MonoBehaviour
{
    [SerializeField] private DialogUI dialogUI;
    private Animator animator;

    public DialogUI DialogUI => dialogUI;
    public IInteracteble interacteble { get; set; }

    private void Start()
    {
        animator = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.R))
        {
            if (interacteble != null) // == interactable?.Interact(this);
            {
                interacteble.Interact(this);
            }
        }

        if (dialogUI.isOpen == true)
        {
            animator.SetBool("talk", true);
        }
        else
        {
            animator.SetBool("talk", false);
        }
        
    }
}
