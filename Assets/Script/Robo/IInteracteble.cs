using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IInteracteble 
{
    void Interact(RoboController roboController);
}
