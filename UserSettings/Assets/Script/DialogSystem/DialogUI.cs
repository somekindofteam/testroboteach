using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class DialogUI : MonoBehaviour
{
    [SerializeField] private TMP_Text textLabel;
    [SerializeField] private AudioSource audioSource;
    [SerializeField] private DialogObject dialogObject;
    [SerializeField] private GameObject dialogueBox;

    private ResponseHandler responseHandler;
    private TypewriterEffect typewriterEffect;

    public bool isOpen { get; set; }

    private void Start()
    {
        typewriterEffect = GetComponent<TypewriterEffect>();
        responseHandler = GetComponent<ResponseHandler>();

        CloseDialogueBox();
        ShowDialogue(dialogObject);
    }

    public void ShowDialogue(DialogObject dialogObject)
    {
        isOpen = true;
        dialogueBox.SetActive(true);
        StartCoroutine(StepThroughDialogue(dialogObject));
    }

    public void AddResponseEvents(ResponseEvent[] responseEvents)
    {
        responseHandler.AddResponseEvents(responseEvents);
    }

    private IEnumerator StepThroughDialogue(DialogObject dialogObject)
    {
        for (int i = 0; i < dialogObject.Dialogs.Length; i++)
        {
            string dialogue = dialogObject.Dialogs[i].DialogText;
            AudioClip audio = dialogObject.Dialogs[i].DialogAudio;

            yield return RunTypingEffect(dialogue);

            textLabel.text = dialogue;
            audioSource.clip = audio;
            audioSource.Play();

            if (i == dialogObject.Dialogs.Length - 1 && dialogObject.HasResponses)
            {
                break;
            }

            yield return null;
            yield return new WaitUntil(() => Input.GetKeyDown(KeyCode.Space));
        }

        if (dialogObject.HasResponses)
        {
            responseHandler.ShowResponses(dialogObject.Responses);
        }else
        {
            CloseDialogueBox();
        }
    }

    private IEnumerator RunTypingEffect(string dialogue)
    {
        typewriterEffect.Run(dialogue, textLabel);

        while (typewriterEffect.isRunning)
        {
            yield return null;

            if (Input.GetKeyDown(KeyCode.Space))
            {
                typewriterEffect.Stop();
            }
        }
    }

    private void CloseDialogueBox()
    {
        isOpen = false;
        dialogueBox.SetActive(false);
        textLabel.text = string.Empty;
    }
}
