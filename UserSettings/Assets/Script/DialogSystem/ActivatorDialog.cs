using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActivatorDialog : MonoBehaviour, IInteracteble
{
    [SerializeField] private DialogObject dialogObject;
    [SerializeField] private RoboController roboController;


    private void Start()
    {
        roboController = GetComponent<RoboController>();
    }

    public void Interact(RoboController robo)
    {
        robo.DialogUI.ShowDialogue(dialogObject);
    }

    public void ActiveDialog()
    {
        roboController.interacteble = this;
    }
}
