using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Dialog/DialogData")]
public class DialogObject : ScriptableObject
{
    //[SerializeField] [TextArea] private string[] DialogText;
    [SerializeField] private Dialog[] dialogs;
    [SerializeField] private Response[] responses;

    //public string[] Dialogue => DialogText;
    public Dialog[] Dialogs => dialogs;
    public Response[] Responses => responses;
    public bool HasResponses => Responses != null && Responses.Length > 0;
}
