using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LoadScene : MonoBehaviour
{
    private Animator animator;
    public int intScene;

    private void Start()
    {
        animator = GetComponent<Animator>();
    }

    public void FadeToScene()
    {
        animator.SetTrigger("fade");
    }

    private void OnFadeComplete()
    {
        SceneManager.LoadScene(intScene);
        Debug.Log(intScene);
    }
}
